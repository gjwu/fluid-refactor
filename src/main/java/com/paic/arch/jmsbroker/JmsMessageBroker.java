package com.paic.arch.jmsbroker;

import org.apache.activemq.broker.BrokerService;

public class JmsMessageBroker {

	private String brokerUrl;
	private BrokerService brokerService;

	public JmsMessageBroker(String brokerUrl) {
		this.brokerUrl = brokerUrl;
	}

	public void create() throws Exception {
		brokerService = new BrokerService();
		brokerService.setPersistent(false);
		brokerService.addConnector(brokerUrl);
	}

	public void start() throws Exception {
		brokerService.start();
	}

	public void stop() throws Exception {
		if (brokerService == null) {
			throw new IllegalStateException("Cannot stop the broker from this API: "
					+ "perhaps it was started independently from this utility");
		}
		brokerService.stop();
		brokerService.waitUntilStopped();
	}

	public String getBrokerUrl() {
		return brokerUrl;
	}

	public void setBrokerUrl(String brokerUrl) {
		this.brokerUrl = brokerUrl;
	}

	public BrokerService getBrokerService() {
		return brokerService;
	}

}
