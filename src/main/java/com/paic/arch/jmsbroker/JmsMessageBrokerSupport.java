package com.paic.arch.jmsbroker;

import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;
import static org.slf4j.LoggerFactory.getLogger;

import org.slf4j.Logger;

public class JmsMessageBrokerSupport {

	private static final Logger LOG = getLogger(JmsMessageBrokerSupport.class);

	public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";

	private JmsMessageManager jmsMessageManager;

	private JmsMessageBrokerSupport(String aBrokerUrl) {
		jmsMessageManager = new JmsMessageManagerImpl(aBrokerUrl);
	}

	public static JmsMessageBrokerSupport createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
		return createARunningEmbeddedBrokerAt(DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000));
	}

	public static JmsMessageBrokerSupport createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {
		LOG.debug("Creating a new broker at {}", aBrokerUrl);
		JmsMessageBrokerSupport broker = bindToBrokerAtUrl(aBrokerUrl);
		broker.createEmbeddedBroker();
		broker.startEmbeddedBroker();
		return broker;
	}

	public static JmsMessageBrokerSupport bindToBrokerAtUrl(String aBrokerUrl) throws Exception {
		return new JmsMessageBrokerSupport(aBrokerUrl);
	}

	public final JmsMessageBrokerSupport andThen() {
		return this;
	}

	private void createEmbeddedBroker() throws Exception {
		jmsMessageManager.createBroker();
	}

	private void startEmbeddedBroker() throws Exception {
		jmsMessageManager.startBroker();
	}

	public void stopTheRunningBroker() throws Exception {
		jmsMessageManager.stopBroker();
	}

	public final String getBrokerUrl() {
		return jmsMessageManager.getBrokerUrl();
	}

	public JmsMessageBrokerSupport sendATextMessageToDestinationAt(String aDestinationName,
			final String aMessageToSend) {
		jmsMessageManager.sendTextMessageToDestination(aDestinationName, aMessageToSend);
		return this;
	}

	public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
		return jmsMessageManager.retrieveTextMessageFromTheDestination(aDestinationName);
	}

	public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) {
		return jmsMessageManager.retrieveTextMessageFromTheDestination(aDestinationName, aTimeout);
	}

	public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
		return jmsMessageManager.getMessageCount(aDestinationName);
	}

	public boolean isEmptyQueueAt(String aDestinationName) throws Exception {
		return getEnqueuedMessageCountAt(aDestinationName) == 0;
	}

	public static class NoMessageReceivedException extends RuntimeException {
		private static final long serialVersionUID = -3885402625249299527L;

		public NoMessageReceivedException(String reason) {
			super(reason);
		}

	}
}
