package com.paic.arch.jmsbroker;

public class JmsMessageManagerImpl implements JmsMessageManager {

	private static final int ONE_SECOND = 1000;
	private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
	private JmsMessageTemplate jmsMessageTemplate;

	public JmsMessageManagerImpl(String brokerUrl) {
		jmsMessageTemplate = new JmsMessageTemplate(brokerUrl);
	}

	@Override
	public void createBroker() throws Exception {
		jmsMessageTemplate.getJmsMessageBroker().create();
	}

	@Override
	public void startBroker() throws Exception {
		jmsMessageTemplate.getJmsMessageBroker().start();
	}

	@Override
	public void stopBroker() throws Exception {
		jmsMessageTemplate.getJmsMessageBroker().stop();
	}

	@Override
	public void bindToBrokerAtUrl(String brokerUrl) {
		jmsMessageTemplate.getJmsMessageBroker().setBrokerUrl(brokerUrl);
	}

	@Override
	public String getBrokerUrl() {
		return jmsMessageTemplate.getJmsMessageBroker().getBrokerUrl();
	}

	@Override
	public void sendTextMessageToDestination(String destinationName, String messageText) {
		jmsMessageTemplate.sendMessage(destinationName, messageText);
	}

	@Override
	public String retrieveTextMessageFromTheDestination(String destinationName) {
		return jmsMessageTemplate.getMessage(destinationName, DEFAULT_RECEIVE_TIMEOUT);
	}

	@Override
	public String retrieveTextMessageFromTheDestination(String destinationName, int timeout) {
		return jmsMessageTemplate.getMessage(destinationName, timeout);
	}

	@Override
	public long getMessageCount(String destinationName) throws Exception {
		return jmsMessageTemplate.getMessageCount(destinationName);
	}

}
