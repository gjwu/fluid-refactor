package com.paic.arch.jmsbroker;

public interface JmsMessageManager {

	void bindToBrokerAtUrl(String brokerUrl);

	String getBrokerUrl();

	void createBroker() throws Exception;

	void startBroker() throws Exception;

	void stopBroker() throws Exception;

	void sendTextMessageToDestination(String destinationName, String messageText);

	String retrieveTextMessageFromTheDestination(String destinationName);

	String retrieveTextMessageFromTheDestination(String destinationName, int timeout);

	public long getMessageCount(String destinationName) throws Exception;

}
