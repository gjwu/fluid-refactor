package com.paic.arch.jmsbroker;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JmsMessageTemplate {
	private static final Logger log = LoggerFactory.getLogger(JmsMessageTemplate.class);

	private JmsMessageBroker jmsMessageBroker;

	private ActiveMQConnectionFactory connectionFactory;

	private Connection connection;

	private Session session;

	public JmsMessageTemplate(String brokerUrl) {
		jmsMessageBroker = new JmsMessageBroker(brokerUrl);
		connectionFactory = new ActiveMQConnectionFactory(brokerUrl);
	}

	private void init() {
		try {
			connection = connectionFactory.createConnection();
			connection.start();
		} catch (JMSException jmse) {
			log.error("failed to create connection to {}", jmsMessageBroker.getBrokerUrl());
			throw new IllegalStateException(jmse);
		}

		try {
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		} catch (JMSException jmse) {
			log.error("Failed to create session on connection {}", connection);
			throw new IllegalStateException(jmse);
		}
	}

	private void close() {
		if (session != null) {
			try {
				session.close();
			} catch (JMSException jmse) {
				log.warn("Failed to close session");
				throw new IllegalStateException(jmse);
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (JMSException jmse) {
				log.warn("Failed to close connection to broker");
				throw new IllegalStateException(jmse);
			}
		}
	}

	public void sendMessage(String destinationName, String messageText) {

		try {
			init();
			
			Queue queue = session.createQueue(destinationName);
			MessageProducer producer = session.createProducer(queue);
			
			producer.send(session.createTextMessage(messageText));
			producer.close();
			
		} catch (Exception e) {
			log.warn("Failed to send message");
			throw new IllegalStateException(e.getMessage());
			
		} finally {
			close();
		}
	}

	public String getMessage(String destinationName, long timeout) {
		
		String msg;
		try {
			init();
			Queue queue = session.createQueue(destinationName);
			MessageConsumer consumer = session.createConsumer(queue);
			Message message = consumer.receive(timeout);
			consumer.close();
			if (message != null) {
				msg = ((TextMessage) message).getText();
			} else {
				throw new JmsMessageBrokerSupport.NoMessageReceivedException(
						String.format("No messages received from the broker within the %d timeout", timeout));
			}
		} catch (JMSException jmse) {
			log.warn("Failed to receive message");
			throw new IllegalStateException(jmse);
		} finally {
			close();
		}
		return msg;
	}

	public long getMessageCount(String destinationName) {
		long count = 0;
		try {
			count = getDestinationStatisticsFor(destinationName).getMessages().getCount();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	private DestinationStatistics getDestinationStatisticsFor(String destinationName) throws Exception {
		Broker regionBroker = jmsMessageBroker.getBrokerService().getRegionBroker();
		for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
			if (destination.getName().equals(destinationName)) {
				return destination.getDestinationStatistics();
			}
		}
		throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", destinationName,
				jmsMessageBroker.getBrokerUrl()));
	}

	public JmsMessageBroker getJmsMessageBroker() {
		return jmsMessageBroker;
	}
}
